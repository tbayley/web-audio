/*
 * JavaScript console override library, v0.1
 * Copyright 2014, Tony Bayley
 *
 * Licensed under the MIT License
 * http://opensource.org/licenses/mit-license.php
 *
 * http://tonybayley.co.uk/console-override
 *
 * Override the standard JavaScript functions that write to the console by 
 * equivalent functions that write to the web browser application window. This
 * is particularly useful for debugging applications on mobile devices that do
 * not have an easily accessible JavaScript console.
 * 
 * To use:  just call initConsole() from the window.onload() function.  If 
 * useBrowserConsole == true, a text areaa will be appended to the application
 * window to display console messages.  To place the console window elsewhere,
 * include an empty div with id="console" at the desired location on the web
 * page.
 */

var initConsole = function (
      useSystemConsole,         // true: print to system console
      useBrowserConsole,        // true: print to browser console
      browserConsoleWidth,      // browser console width (characters)
      browserConsoleHeight) {   // browser console height (lines)
    // add a debug console to browser window, if required
    if (useBrowserConsole) {
        // get the empty 'console' div, if it exists
        var console_div = document.getElementById('console');
        if (console_div === null) {
            // otherwise append a 'console' div to the end of the document
            var body = document.getElementsByTagName('body') [0];
            console_div = document.createElement('div');
            var textNode = document.createTextNode('Creating Div Element')
            console_div.appendChild(textNode);
            body.appendChild(console_div);
        }
        console_div.innerHTML='<br><p>Debug Print Console</p>' +
            '<textarea id="console-text-area" rows="' + 
            browserConsoleHeight +'" cols="' + 
            browserConsoleWidth + '" readonly></textarea>';
    }
    
    //save the original console functions
    var systemConsoleLog = console.log;
    var systemConsoleInfo = console.info;
    var systemConsoleWarn = console.warn;
    var systemConsoleError = console.error;
 
    //overriding console.log function
    console.log = function() {
        print(systemConsoleLog, '', arguments);
    };


    //overriding console.info function
    console.info = function() {
        print(systemConsoleInfo, 'INFO: ', arguments);
    };


    //overriding console.warn function
    console.warn = function() {
        print(systemConsoleWarn, 'WARN: ', arguments);
    };


    //overriding console.error function
    console.error = function() {
        print(systemConsoleError, 'ERROR: ', arguments);
    };
    

    //overriding console functions
    var print = function(systemConsoleFunction, prefix, args) {
        if (useSystemConsole) {
            // write to system console
            systemConsoleFunction.apply(console, args);
        }
        if(useBrowserConsole){
            // write to browser window
            var msg = prefix;
            for (var i=0; i<args.length; i++) {
                msg += String(args[i]);
            }
            var browserConsole = document.getElementById('console-text-area');
            browserConsole.value += msg + '\n';
        }
    };
    
};
   
