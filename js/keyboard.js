var settings = {
        id: 'keyboard',
        width: 800,
        height: 200,
        startNote: 'A4',
        whiteNotesColour: 'white',
        blackNotesColour: 'black',
        hoverColour: 'yellow',
        octaves: 2
    };
var keyboard = new QwertyHancock(settings);
var context = new AudioContext();
var waveform = 'square';
var masterGain = context.createGain();
var nodes = [];

masterGain.gain.value = 0.3;
masterGain.connect(context.destination); 

keyboard.keyDown = function (note, frequency) {
    console.info('Play note: ' + note + ' (' + frequency.toFixed(3) + ' Hz)');
    var oscillator = context.createOscillator();
    oscillator.type = waveform;
    oscillator.frequency.value = frequency;
    oscillator.connect(masterGain);
    oscillator.start(0);
    nodes.push(oscillator);
};

keyboard.keyUp = function (note, frequency) {
    var new_nodes = [];
    for (var i = 0; i < nodes.length; i++) {
        if (Math.round(nodes[i].frequency.value) === Math.round(frequency)) {
            nodes[i].stop(0);
            nodes[i].disconnect();
        } else {
            new_nodes.push(nodes[i]);
        }
    }
    nodes = new_nodes;
};

var setWaveform = function (w) {
    waveform = w;
};

window.onload = function () {
    // print to browser window instead of system console
    initConsole(true, true, 80, 8);
};
